from django.utils import timezone
from django.views.generic.detail import DetailView
from django.contrib.auth.models import User
from django.http import HttpResponse
import pdfkit
from django.template.loader import render_to_string
from xhtml2pdf import pisa
import io as StringIO
from time import sleep
from django.conf import settings
from django.contrib.staticfiles import finders
import os

def link_callback(uri, rel):
    """
    Convert HTML URIs to absolute system paths so xhtml2pdf can access those
    resources
    """
    result = finders.find(uri)
    if result:
            if not isinstance(result, (list, tuple)):
                    result = [result]
            result = list(os.path.realpath(path) for path in result)
            path=result[0]
    else:
            sUrl = settings.STATIC_URL        # Typically /static/
            sRoot = settings.STATIC_ROOT      # Typically /home/userX/project_static/
            mUrl = settings.MEDIA_URL         # Typically /media/
            mRoot = settings.MEDIA_ROOT       # Typically /home/userX/project_static/media/

            if uri.startswith(mUrl):
                    path = os.path.join(mRoot, uri.replace(mUrl, ""))
            elif uri.startswith(sUrl):
                    path = os.path.join(sRoot, uri.replace(sUrl, ""))
            else:
                    return uri

    # make sure that file exists
    if not os.path.isfile(path):
            raise Exception(
                    'media URI must start with %s or %s' % (sUrl, mUrl)
            )
    return path

class UserDetailView(DetailView):
    model = User
    template_name = 'conceptos/detail.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["now"] = timezone.now()
        return context
    
    def render_to_response(self, context, **response_kwargs):
        context['url'] = self.request.get_host()
        html = render_to_string(self.get_template_names(), context)
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'inline; filename="Report.pdf"'
        pisa_status = pisa.CreatePDF(html, dest=response, link_callback=link_callback)
        if pisa_status.err:
            return HttpResponse('We had some errors <pre>' + html + '</pre>')
        return response
### Leer esto
# https://github.com/JazzCore/python-pdfkit/wiki/Installing-wkhtmltopdf
# y debes agregar en variables de entorno/sistema:
# C:\Program Files\wkhtmltopdf\bin
# o la ruta equivalente
